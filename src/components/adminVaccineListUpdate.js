import React, { Component } from 'react';  
import axios from 'axios';

class Table extends Component {  
  constructor(props) {  
    super(props);
    }   
    
    deleteVaccineStock= () =>{   
      var {jwtToken} = JSON.parse(localStorage.getItem("admin") ? localStorage.getItem("admin"): localStorage.getItem("user-info"));
      axios.delete('https://localhost:44335/Api/VaccineList/DeleteVaccine/?id='+this.props.obj.id, {
        headers: {
          "Authorization": `Bearer ${jwtToken}`
        }
      })  
      .then(json => {  
      if(json.data.Status==='Delete'){    
        window.location.reload(false);
      }  
      }) 
    }  

  render() {  
    return (  
        <tr style={{ textAlign: "center" }}>  
          <td>  
            {this.props.obj.availVaccineName}
          </td>  
          <td>  
            {this.props.obj.limit}
          </td>
          <td>  
          <button type="button" class="btn btn-danger" onClick={this.deleteVaccineStock}>Delete</button>  
          </td> 
        </tr>  
    );  
  }  
}  

export default Table;  