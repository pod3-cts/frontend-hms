import React from "react"
import axios from "axios";
import AppointmentTable from './eventShowTable'

export default class AppointmentShowcase extends React.Component{
    
    constructor(props) {  
        super(props);  
        this.state = {listOfAppointment: []};  
      }  
      componentDidMount(){  
        debugger;  
        var {jwtToken} = JSON.parse(localStorage.getItem("admin") ? localStorage.getItem("admin"): localStorage.getItem("user-info"));
        axios.get('https://localhost:44335/Api/Appoitment/Appointmentdetails', {
          headers: {
            "Authorization": `Bearer ${jwtToken}`
          }
        })  
          .then(response => {  
            this.setState({ listOfAppointment: response.data });  
            debugger; 
  
          })  
          .catch(function (error) {  
            console.log(error);  
          })  
      }  
  
      appointmentRow(){  
        return this.state.listOfAppointment.map(function(object, i){  
            return <AppointmentTable obj={object} key={i} />;  
        });  
      }

    render(){
    return(
    <div id="showcaseform">
      <h4 id="headerTitle">Appointments Received</h4>
          <table className="table table-striped" style={{ marginTop: 10 }}>  
            <thead>  
              <tr style={{ textAlign: "center" }}>  
                <th>User</th>  
                <th>Date</th>  
                <th>Problem Description</th>  
                <th>Doctor Speciality</th>  
                <th>Counseled before?</th>  
                <th colSpan="4">Action</th>  
              </tr>  
            </thead>  
            <tbody>  
             { this.appointmentRow() }   
            </tbody>  
          </table>    
    </div>
  )
}
}