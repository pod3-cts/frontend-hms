import React from 'react';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import { BrowserRouter, Route} from "react-router-dom";
import LoginPatient from './components/loginPatient'
import SignUpPatient from './components/signupPatient'
import Navigation from './components/navigation'
import HomePage from './components/home'
import VaccinationCalendar from './components/calendarVaccination';
import AppointmentCalendar from './components/calendarAppointment';
import Driver from './components/driverSeperator';
import LoginAdmin from './components/loginAdmin';
import EventTable from './components/eventShowTable'
import Showcase from './components/adminShowcase';
import ShowcaseVaccine from './components/adminShowcaseVaccination';
import Help from './components/Help'
import DocEntry from './components/doctorEntry'
import DocAdminEntry from './components/adminDocEntry'
import StockUpdate from './components/stockUpdate'
import StockEntry from './components/stockForm'
import MailAppointment from './components/mailAppointment'
import MailVaccination from './components/mailVaccination'
import BlodEntry from './components/bloodEntry'
import BlodAdminEntry from './components/bloodDocEntry'
import Charts from './components/chart'


function App() {
  return (        
      <div className = "App">
        <div>
        <Navigation />        
        <BrowserRouter>
          <Route exact path = '/' component = {HomePage} />
          <Route path = '/components/home' component = {HomePage} />
          <Route path = '/components/loginPatient' component = {LoginPatient} /> 
          <Route path = '/components/signupPatient' component = {SignUpPatient} />
          <Route path = '/components/loginAdmin' component = {LoginAdmin} /> 
          <Route path = "/components/driverSeperator" component = {Driver}/>
          <Route path = '/components/calendarAppointment' component = {AppointmentCalendar} />
          <Route path = '/components/calendarVaccination' component = {VaccinationCalendar} />
          <Route path = "/components/eventShowTable" component = {EventTable}/>
          <Route path = "/components/adminShowcase" component = {Showcase}/>
          <Route path = "/components/adminShowcaseVaccination" component = {ShowcaseVaccine}/>
          <Route path = "/components/doctorEntry" component = {DocEntry}/>
          <Route path = "/components/adminDocEntry" component = {DocAdminEntry}/>
          <Route path = "/components/stockUpdate" component = {StockUpdate}/>
          <Route path = "/components/stockForm" component = {StockEntry}/>
          <Route path = "/components/Help" component = {Help}/>
          <Route path = "/components/mailAppointment/:id" component = {MailAppointment}/>
          <Route path = "/components/mailVaccination/:id" component = {MailVaccination}/>
          <Route path = "/components/bloodEntry" component = {BlodEntry}/>
          <Route path = "/components/bloodDocEntry" component = {BlodAdminEntry}/>
          <Route path = "/components/chart" component = {Charts}/>
        </BrowserRouter>
        </div>
      </div>
  );
}

export default App;
