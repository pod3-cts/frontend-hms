import React from "react"
import axios from "axios";
import VacUpdateList from './adminVaccineListUpdate'

export default class VaccineUpdate extends React.Component{
    
    constructor(props) {  
        super(props);  
        this.state = {listOfVaccine: []};  
      }  
      componentDidMount(){  
        debugger; 
        var {jwtToken} = JSON.parse(localStorage.getItem("admin") ? localStorage.getItem("admin"): localStorage.getItem("user-info"));
        axios.get('https://localhost:44335/Api/VaccineList/VaccineList', {
          headers: {
            "Authorization": `Bearer ${jwtToken}`
          }
        })  
          .then(response => {  
            this.setState({ listOfVaccine: response.data });  
             
          })  
          .catch(function (error) {  
            console.log(error);  
          })  
      }  
  
      deleteVaccineList(){  
        return this.state.listOfVaccine.map(function(object, i){  
            return <VacUpdateList obj={object} key={i} />;  
        });  
      }

    render(){
    return(
    <div id="showcaseform">
      <h4 id="headerTitle">Stock Update</h4>
          <table className="table" style={{ marginTop: 10 }}>  
            <thead>  
              <tr style={{ textAlign: "center" }}>  
                <th>Vaccine Name</th>  
                <th>Vaccine Availability</th>  
                <th colSpan="4">Action</th>  
              </tr>  
            </thead>  
            <tbody>  
             { this.deleteVaccineList() }   
            </tbody>  
          </table>    
    </div>
  )
}
}