import React, { Component } from 'react';
import axios from 'axios';
import {Link} from 'react-router-dom'

class Table extends Component {
  constructor(props) {
    super(props);
  }

  DeleteAppointment = (e) => {
    
    var {jwtToken} = JSON.parse(localStorage.getItem("admin") ? localStorage.getItem("admin"): localStorage.getItem("user-info"));
    axios.delete('https://localhost:44335/Api/Appoitment/Deleteappointment?Id=' + this.props.obj.id, {
      headers: {
        "Authorization": `Bearer ${jwtToken}`
      }
    })
      .then(json => {
        if (json.data.Status === 'Delete') {
          window.location.reload(false);
        }
      })
  }

  render() {
    return (
      <tr style={{ textAlign: "center" }}>
        <td name = "email">
          {this.props.obj.userMail}
        </td>
        <td name="date">
          {this.props.obj.confrimdate}
        </td>
        <td name="ProbDesc">
          {this.props.obj.problemdescription}
        </td>
        <td name="DocType">
          {this.props.obj.doctortype}
        </td>
        <td name="CounselStatus">
          {this.props.obj.counseledbefore}
        </td>
        <td>
          {/* mail functionality */}
          <button type="button" class="btn btn-success" ><Link style={{ textDecoration: "none", color: 'white'}} to = {"/components/mailAppointment/"+this.props.obj.id} >Accept</Link></button>
        </td>
        <td>
          <button type="button" class="btn btn-danger" onClick={this.DeleteAppointment} >Delete</button>
        </td>
      </tr>
    );
  }
}

export default Table;