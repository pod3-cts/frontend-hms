import React, { Component } from 'react';  
import axios from 'axios';

class Table extends Component {  
  constructor(props) {  
    super(props);  
    }   
    doctorAvailability(){
        axios.post('https://localhost:44335/Api/Doctor/AddDoctor')  
        .then(json => {  
        if(json.data.Status ==='Success'){  
          alert('Doctor Entry Successfull...');
        }  
        })
    }

  render() {  
    return (  
        <tr style={{ textAlign: "center" }}>  
          <td>  
            {this.props.obj.doctorName}  
          </td>  
          <td>  
            {this.props.obj.doctorSpecilaity}  
          </td>
          <td>  
            {this.props.obj.doctorAvilability}  
          </td> 
        </tr>  
    );  
  }  
}  

export default Table;  