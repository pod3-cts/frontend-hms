import React from 'react';
import './calendarAppointment';
import { Link } from 'react-router-dom'
import './homeAndDashboard.scss'
import Powerslap from "./media/Powerslap.mp4"

function Seperator() {
  const user = JSON.parse(localStorage.getItem('user-info'));

  return (
    <div id='driverseperatordesign'>
      <video autoPlay loop muted style={{
        width: "100%", left: "100%",
        top: "100%", height: "100%", objectFit: "cover", transofrm: "translate(-50%,-50%)",
        zIndex: "-1"
      }}>
        <source src={Powerslap} type="video/mp4" />
      </video>
      <div class = 'content'>
        <h4>Welcome,</h4> <HomePageText title={user.name} />
        <div>
          <FormButtonAppointment title="Appointment" />
        </div>
        <div>
          <FormButtonVaccination title="Vaccination" />
        </div>
      </div>
    </div>
  )
}

const HomePageText = props => (
  <h1>{props.title}</h1>
);

const FormButtonAppointment = props => (
  <button id="buttonA" class="offset">
    <Link to="./calendarAppointment" style={{ textDecoration: "none", color: 'black' }}>
      {props.title}
    </Link>
  </button>
);

const FormButtonVaccination = props => (
  <button id="buttonV" class="offset">
    <Link to="./calendarVaccination" style={{ textDecoration: "none", color: 'black'}}>
      {props.title}
    </Link>
  </button>
);

//ReactDOM.render(<App />, document.getElementById('container'));
export default Seperator;