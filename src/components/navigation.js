import { Navbar, Nav, Container, NavDropdown } from 'react-bootstrap';
import {useHistory} from 'react-router-dom'

function Navigation() {
    const history = useHistory();
    const user = JSON.parse(localStorage.getItem('user-info'));
    function Logout() {
        localStorage.removeItem('user-info');
        sessionStorage.removeItem('User');
        history.push('/components/home');
        window.location.reload(false);
    }

    function AdminLogout(){
        localStorage.removeItem('admin');
        history.push('/components/home');
        window.location.reload(false);
    }

    return (
        <>
            <Navbar id="navbarScrollingDropdown" collapseOnSelect fixed = "top" expand = 'sm' variant = 'light'> 
                <Container>
                    <Navbar.Brand href="/components/home">Hospital Management System</Navbar.Brand>
                        {localStorage.getItem('admin') ?
                        <Nav className="ml-auto nav-bar-wrapper">
                            <Navbar.Collapse id = 'responsive-navbar-nav'></Navbar.Collapse>
                            <NavDropdown className="mr-auto nav-bar-wrapper" title = {"Blood Bank"}>
                                <NavDropdown.Item href = '/components/chart'>Pie Chart</NavDropdown.Item>
                                <NavDropdown.Item href = '/components/bloodEntry'>Blood Entry</NavDropdown.Item>
                                <NavDropdown.Item href = '/components/bloodDocEntry'>Blood Availability</NavDropdown.Item>
                            </NavDropdown>
                            <NavDropdown className="mr-auto nav-bar-wrapper" title = {"Appointments"}>
                                <NavDropdown.Item href = '/components/adminShowcase'>Request List</NavDropdown.Item>
                                <NavDropdown.Item href = '/components/doctorEntry'>Doctor Entry</NavDropdown.Item>
                                <NavDropdown.Item href = '/components/adminDocEntry'>Today's Doctor</NavDropdown.Item>
                            </NavDropdown>
                            <NavDropdown className="mr-auto nav-bar-wrapper" title = {"Vaccination"}>
                                <NavDropdown.Item href = '/components/adminShowcaseVaccination'>Request List</NavDropdown.Item>
                                <NavDropdown.Item href = '/components/stockUpdate'>Stock Remove</NavDropdown.Item>
                                <NavDropdown.Item href = '/components/stockForm'>Stock Entry</NavDropdown.Item>
                            </NavDropdown>
                            <Navbar.Collapse/>

                            <NavDropdown className="mr-auto nav-bar-wrapper" title = {"Admin"}>
                                <NavDropdown.Item onClick = {AdminLogout}>Logout</NavDropdown.Item>
                            </NavDropdown>
                        </Nav>
                        :
                        <Navbar.Collapse id = 'responsive-navbar-nav'>
                            <Nav className="ml-auto nav-bar-wrapper">
                                <Nav.Link href = '/components/loginAdmin'>Admin</Nav.Link>
                                <Nav.Link href = '/components/Help'>Help</Nav.Link>
                            </Nav>
                        </Navbar.Collapse>
                        }
                        {localStorage.getItem('user-info') ?
                            <Nav>
                                <NavDropdown title = {user && user.name}>
                                    <NavDropdown.Item href = '/components/driverSeperator'>Dashboard</NavDropdown.Item>
                                    <NavDropdown.Item onClick = {Logout}>Logout</NavDropdown.Item>
                                </NavDropdown>
                            </Nav>
                            : null
                        }
                </Container>
            </Navbar>
        </>
    );
}

export default Navigation;