import React,{useState,useEffect} from "react"
import {useHistory} from 'react-router-dom'
import "./loginPatient.css";

function LoginFormPatient() {
  const[Email,setEmail]=useState("");
  const[Password,setPassword]=useState("");
  const [EmailErr, setEmailErr] = useState("");
  const [PasswordErr, setPasswordErr] = useState("");

  const history = useHistory();
  useEffect(()=>{
  if(localStorage.getItem('user-info')){
    history.push("/components/driverSeperator")
  }
  }, []);

    const formValidation = () => {
        const EmailErr = {};
        const PasswordErr = {};

        let isValid = true;

        if (!Email) {
            EmailErr.nullEmail = "Email id is required";
            isValid = false;
        }
        else if (!(/^\w+([\.-]?\w+)@\w+([\.-]?\w+)(\.\w{2,3})+$/.test(Email))) {
            EmailErr.invalidEmail = "Invalid EmailID";
            isValid = false;
        }
        if (!Password) {
            PasswordErr.nullPwd = "Password is Required";
            isValid = false;
        }
        else if (!(/(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/.test(Password))) {
            PasswordErr.invalidPwd = "Use 8 or more characters with a mix of Letters & Numbers.";
            isValid = false;
        }
        setEmailErr(EmailErr);
        setPasswordErr(PasswordErr);

        return isValid;
    }

    async function login() {
        const isValid = formValidation();
        if (isValid) {
           let item = {Email,Password}
           sessionStorage.setItem('User', JSON.stringify(item.Email));
           console.log(item);
           let result = await fetch("https://localhost:44335/Api/login/PatientLogin",{
               method:'POST',
               headers:{
                  "Content-Type":"application/json",
                  "Accept":"application/json"
               },
               body:JSON.stringify(item)
           });

           result = await result.json();
           localStorage.setItem('user-info',JSON.stringify(result));
           var checkLogin = JSON.parse(localStorage.getItem('user-info'));
           if(expect(checkLogin.value).toEqual("null")){
               history.push('/components/loginPatient');
               window.location.reload(true);
           }
           history.push("/components/driverSeperator");
        }
        else {
            alert("Kindly fill the details correctly!!!")
        }
    }

  return(
    <div id="loginform">
      <form>
        <h2 id="headerTitle">Getting you cured...</h2>
        <div class = 'row'>
            <label>Email</label>
            <input type="email" value={Email} onChange={(e) => setEmail(e.target.value)} placeholder="Enter your email ID" />
            {Object.keys(EmailErr).map((key) => {
                return <div style={{ color: "red" }}>{EmailErr[key]}</div>
             })}
        </div>

        <div class = 'row' >
             <label>Password</label>
             <input type="password" value={Password} onChange={(e) => setPassword(e.target.value)} placeholder="Enter your password" />
             {Object.keys(PasswordErr).map((key) => {
                 return <div style={{ color: "red" }}>{PasswordErr[key]}</div>
             })}
        </div>

        <div class = 'row' id = 'button'>
          <button type="submit" onClick={login}>Submit</button>
        </div>

        <p  className="text-left">
        New <a href="./signupPatient">User?</a>
        </p>
      </form>
    </div>
  )
}


//ReactDOM.render(<App />, document.getElementById('container'));
export default LoginFormPatient;