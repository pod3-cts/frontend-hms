import React, { Component } from "react";
import Chart from "react-apexcharts";

class Charts extends Component {
  constructor(props) {
    super(props);

    this.state = {
      options: {
        chart: {
          id: "basic-bar"
        },
        xaxis: {
          categories: [2009, 2010, 2012, 2014, 2016, 2018, 2020, 2022]
        }
      },
      series: [
        {
          name: "Users",
          data: [197, 289, 745, 950, 1049, 1430, 1670,2091]
        }
      ]
      
    };
  }

  render() {
    return (
        
      <center>
          <br></br>
        <br></br>
        <div className="app">
        <div className="row">
          <div className="mixed-chart">
          <Chart
    options={this.state.options}
    series={this.state.series}
    type="line"
    width="500"
  />
          </div>
        </div>
      </div>
      </center>
    );
  }
}

export default Charts;