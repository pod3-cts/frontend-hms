import "./signupPatient.css";
import React, { useState, useEffect } from "react"
import { useHistory } from 'react-router-dom'

function SignUpFormPatient() {
  const [FirstName, setFName] = useState("");
  const [LastName, setLName] = useState("");
  const [Email, setEmail] = useState("");
  const [Mobile, setMobile] = useState("");
  const [Gender, setGender] = useState("");
  const [Age, setAge] = useState("");
  const [Password, setPassword] = useState("");
  const [ConfirmPassword, setCPassword] = useState("");

  const [FirstNameErr, setFNameErr] = useState("");
  const [LastNameErr, setLNameErr] = useState("");
  const [EmailErr, setEmailErr] = useState("");
  const [MobileErr, setMobileErr] = useState("");
  const [GenderErr, setGenderErr] = useState("");
  const [AgeErr, setAgeErr] = useState("");
  const [PasswordErr, setPasswordErr] = useState("");
  const [ConfirmPasswordErr, setCPasswordErr] = useState("");

  const history = useHistory();
  useEffect(() => {
    if (localStorage.getItem('user-info')) {
      history.push("/components/driverSeperator")
    }
  }, []);

  const formValidation = () => {
    const FirstNameErr = {};
    const LastNameErr = {};
    const EmailErr = {};
    const MobileErr = {};
    const GenderErr = {};
    const AgeErr = {};
    const PasswordErr = {};
    const ConfirmPasswordErr = {};

    let isValid = true;

    if (!FirstName) {
      FirstNameErr.nullFN = "First Name is required";
      isValid = false;
    }
    if (!LastName) {
      LastNameErr.nullLN = "Last Name is required";
      isValid = false;
    }
    if (!Email) {
      EmailErr.nullEmail = "Email id is required";
      isValid = false;
    }
    else if (!(/^\w+([\.-]?\w+)@\w+([\.-]?\w+)(\.\w{2,3})+$/.test(Email))) {
      EmailErr.invalidEmail = "Invalid EmailID";
      isValid = false;
    }
    if (!Mobile) {
      MobileErr.nullMobile = "Phone Number is required";
      isValid = false;
    }
    else if (isNaN(Mobile)) {
      MobileErr.invalidMobile = "Number should be Numeric";
      isValid = false;
    }
    if (!Gender) {
      GenderErr.nullGender = "Please enter the Gender";
      isValid = false;
    }
    if (!Age) {
      AgeErr.nullAge = "Age is required";
      isValid = false;
    }
    else if (isNaN(Age)) {
      AgeErr.notNumAge = "The Age must be a number";
      isValid = false;
    }
    if (!Password) {
      PasswordErr.nullPwd = "Password is Required";
      isValid = false;
    }
    else if (!(/(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/.test(Password))) {
      PasswordErr.invalidPwd = "Use 8 or more characters with a mix of Letters, Numbers & Symbols";
      isValid = false;
    }
    if (!ConfirmPassword) {
      ConfirmPasswordErr.nullCPwd = "Please re-enter the Password";
      isValid = false;
    }
    else if (Password != ConfirmPassword) {
      ConfirmPasswordErr.mismatchPwd = "Passwords do not match";
      isValid = false;
    }
    setFNameErr(FirstNameErr);
    setLNameErr(LastNameErr);
    setEmailErr(EmailErr);
    setMobileErr(MobileErr);
    setGenderErr(GenderErr);
    setAgeErr(AgeErr);
    setPasswordErr(PasswordErr);
    setCPasswordErr(ConfirmPasswordErr);

    return isValid;
  }

  function handleSubmit(event) {
    event.preventDefault();
  }

  async function signUp() {
    const isValid = formValidation();
    if (isValid) {
      let item = { FirstName, LastName, Email, Mobile, Gender, Age, Password }
      let result = await fetch("https://localhost:44335/Api/login/InsertPatient", {
        method: 'POST',
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json"
        },
        body: JSON.stringify(item)
      });

      result = await result.json();

      history.push("/components/loginPatient");
    }
  }

  return (
    <div id="signupform">
      <form onSubmit={handleSubmit}>
        <h2 id="headerTitle">Welcome Mr and Mrs...</h2>
        <div id="leftHalfSignUp">
          <div class='row'>
            <label>First Name</label>
            <input type="text" value={FirstName} onChange={(e) => setFName(e.target.value)} placeholder="Enter your first name" />
            {Object.keys(FirstNameErr).map((key) => {
              return <div style={{ color: "red" }}>{FirstNameErr[key]}</div>
            })}
          </div>

          <div class='row'>
            <label>Last Name</label>
            <input type="text" value={LastName} onChange={(e) => setLName(e.target.value)} placeholder="Enter your last name" />
            {Object.keys(LastNameErr).map((key) => {
              return <div style={{ color: "red" }}>{LastNameErr[key]}</div>
            })}
          </div>

          <div class='row' >
            <label>Email</label>
            <input type="email" value={Email} onChange={(e) => setEmail(e.target.value)} placeholder="Enter your Email Id" />
            {Object.keys(EmailErr).map((key) => {
              return <div style={{ color: "red" }}>{EmailErr[key]}</div>
            })}
          </div>

          <div class='row'>
            <label>Phone Number</label>
            <input type="text" value={Mobile} onChange={(e) => setMobile(e.target.value)} placeholder="Enter your phone number" />
            {Object.keys(MobileErr).map((key) => {
              return <div style={{ color: "red" }}>{MobileErr[key]}</div>
            })}
          </div>


        </div>

        <div id="rightHalfSignUp">
          <div class='row'>
          <label>Gender</label>
                      <select name="gen" id="gen"  onChange={(e)=>setGender(e.target.value)}>
                        <option selected disabled hidden value="none">Select an Option</option>
                        <option value="Male">Male</option>
                        <option value="Female">Female</option>
                        <option value="Transgender">Transgender</option>
                        <option value="I prefer not to say.">I prefer not to say.</option>
                  </select>
                  {Object.keys(GenderErr).map((key) => {
              return <div style={{ color: "red" }}>{GenderErr[key]}</div>
            })}
          </div>


          <div class='row' >
            <label>Age</label>
            <input type="text" value={Age} onChange={(e) => setAge(e.target.value)} placeholder="Enter your age" />
            {Object.keys(AgeErr).map((key) => {
              return <div style={{ color: "red" }}>{AgeErr[key]}</div>
            })}
          </div>

          <div class='row' >
            <label>Password</label>
            <input type="password" value={Password} onChange={(e) => setPassword(e.target.value)} placeholder="Enter your password" />
            {Object.keys(PasswordErr).map((key) => {
              return <div style={{ color: "red" }}>{PasswordErr[key]}</div>
            })}
          </div>

          <div class='row' >
            <label>Confirm Password</label>
            <input type="password" value={ConfirmPassword} onChange={(e) => setCPassword(e.target.value)} placeholder="Confirm your password" />
            {Object.keys(ConfirmPasswordErr).map((key) => {
              return <div style={{ color: "red" }}>{ConfirmPasswordErr[key]}</div>
            })}
          </div>
        </div>

        <div id='registerButton'>
          <div id="button" onClick={signUp} class="row">
            <button type='submit'>Register</button>
          </div>
        </div>
      </form>
    </div>
  )
}


// ReactDOM.render(<App />, document.getElementById('container'));
export default SignUpFormPatient;