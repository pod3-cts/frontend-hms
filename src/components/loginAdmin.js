import React, { useState, useEffect } from "react"
import { useHistory } from 'react-router-dom'
import "./loginDoctor.css";

function LoginFormAdmin() {
  const [Email, setEmail] = useState("");
  const [Password, setPassword] = useState("");

  const [EmailErr, setEmailErr] = useState("");
  const [PasswordErr, setPasswordErr] = useState("");

  const history = useHistory();
  useEffect(() => {
    if (localStorage.getItem('admin')) {
      history.push("/components/adminShowcase")
    }
  }, []);

  const formValidation = () => {
    const EmailErr = {};
    const PasswordErr = {};

    let isValid = true;

    if (!Email) {
      EmailErr.nullEmail = "Email id is required";
      isValid = false;
    }
    else if (!(/^\w+([\.-]?\w+)@\w+([\.-]?\w+)(\.\w{2,3})+$/.test(Email))) {
      EmailErr.invalidEmail = "Invalid EmailID";
      isValid = false;
    }
    if (!Password) {
      PasswordErr.nullPwd = "Password is Required";
      isValid = false;
    }
    else if (!(/(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/.test(Password))) {
      PasswordErr.invalidPwd = "Use 8 or more characters with a mix of Letters & Numbers.";
      isValid = false;
    }
    setEmailErr(EmailErr);
    setPasswordErr(PasswordErr);

    return isValid;
  }

  async function login() {
    const isValid = formValidation();
    if (isValid) {
      let item = { Email, Password }
      let result = await fetch("https://localhost:44335/Api/Admin/AdminLogin", {
        method: 'POST',
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json"
        },
        body: JSON.stringify(item)
      });
      result = await result.json();
      localStorage.setItem('admin', JSON.stringify(result));
      var checkLogin = JSON.parse(localStorage.getItem('admin'));
      if (expect(checkLogin.value).toEqual("null")) {
        history.push('/components/adminShowcase');
        window.location.reload(false);
      }
      history.push("/components/loginAdmin");
    }
    else {
      alert("Kindly fill the details correctly!!!")
    }
  }

  return (
    <div id="loginform">
      <form>
        <h2 id="headerTitle">Hello Admin...</h2>
        <div class='row'>
          <label>Admin Email</label>
          <input type="email" onChange={(e) => setEmail(e.target.value)} placeholder="Enter email..." />
          {Object.keys(EmailErr).map((key) => {
            return <div style={{ color: "red" }}>{EmailErr[key]}</div>
          })}
        </div>

        <div class='row' >
          <label>Password</label>
          <input type="password" onChange={(e) => setPassword(e.target.value)} placeholder="Enter password..." />
          {Object.keys(PasswordErr).map((key) => {
            return <div style={{ color: "red" }}>{PasswordErr[key]}</div>
          })}
        </div>

        <div class='row' id='button'>
          <button type="submit" onClick={login}>Submit</button>
        </div>
      </form>
    </div>
  )
}


//ReactDOM.render(<App />, document.getElementById('container'));
export default LoginFormAdmin;