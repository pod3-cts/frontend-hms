import React from "react"
import emailjs from "emailjs-com";
import axios from "axios";


export default class MailForm extends React.Component {
    constructor(props) {
        super(props);

        this.onChangeMail = this.onChangeMail.bind(this);
        this.onChangeDate = this.onChangeDate.bind(this);
        this.onChangeSlot = this.onChangeSlot.bind(this);

        this.state = {
            UserMail: '',
            ConfrimDateOfVaccination: '',
            ChooseSlot:''
        }
    }
    componentDidMount() {
        console.log(this.props.match.params.id);
        axios.get('https://localhost:44335/Api/Vaccine/VaccinationById/'+this.props.match.params.id)
            .then(response => {
                console.log(response.data)
                this.setState({
                    UserMail: response.data.userMail,
                    ConfrimDateOfVaccination: response.data.confrimDateOfVaccination,
                    ChooseSlot:response.data.chooseSlot
                });

            })
            .catch(function (error) {
                console.log(error);
            })
    }

    onChangeMail(e) {
        this.setState({
            UserMail: e.target.value
        });
    }
    
    onChangeDate(e) {
        this.setState({
            ConfrimDateOfVaccination: e.target.value
        });
    }

    onChangeSlot(e) {
        this.setState({
            ChooseSlot: e.target.value
        });
    }

    sendEmail(e) {
        e.preventDefault();

        emailjs.sendForm('service_wbvmblm', 'template_9fzs1p8', e.target, 'jYyCjFqvZ3DLOKzks')
            .then((result) => {
                alert("Successfully mail sent!");
            }, (error) => {
                alert("Mail failed to send!!");
            });
        e.target.reset()
    }
    render() {
        return (
            <div id="helpform">
                <form onSubmit = {this.sendEmail}>
                    <h2 id="headerTitle">Are you sure to confirm?!</h2>
                    <div class='row'>
                        <label>EmailID</label>
                        <input style={{color:'orange'}} type="email" name="email" value={this.state.UserMail} onChange={(e) => this.onChangeMail} />
                    </div>

                    <div class='row' >
                        <label>Date of Vaccination</label>
                        <input style={{color:'orange'}} type="text" name="date" value={this.state.ConfrimDateOfVaccination} onChange={(e) => this.onChangeDate} />
                    </div>

                    <div class='row' >
                        <label>Slot</label>
                        <input style={{color:'orange'}} type = "text" name="slot" value={this.state.ChooseSlot} onChange={(e) => this.onChangeSlot}/>
                    </div>

                    <div>
                        <button class='offset' id='button2' type="submit">Send Mail</button>
                    </div>
                </form>
            </div>
        )
    }
}


//ReactDOM.render(<App />, document.getElementById('container'));