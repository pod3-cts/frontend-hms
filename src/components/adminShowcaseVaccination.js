import React from "react"
import axios from "axios";
import VaccinationTable from './vaccineShowTable'

export default class AdminShowcaseVaccine extends React.Component{
    
    constructor(props) {  
        super(props);  
        this.state = {listOfVaccination: []};  
      }  
      componentDidMount(){  
        debugger;  
        axios.get('https://localhost:44335/Api/Vaccine/VaccinationDeatils')  
          .then(response => {  
            this.setState({ listOfVaccination: response.data });  
            debugger; 
  
          })  
          .catch(function (error) {  
            console.log(error);  
          })  
      }  
      
      vaccinationRow(){  
        return this.state.listOfVaccination.map(function(object, i){  
            return <VaccinationTable obj={object} key={i} />;  
        });  
      }

    render(){
    return(
    <div id="showcaseform">
        <h4 id="headerTitle">Vaccine Slot Requests</h4>
            <table className="table table-striped" style={{ marginTop: 10 }}>  
                <thead>  
                    <tr style={{ textAlign: "center" }}>  
                    <th>User</th>  
                    <th>Date</th>  
                    <th>Vaccine Name</th>  
                    <th>Slot</th>  
                    <th>ID Proof</th>  
                    <th colSpan="4">Action</th>  
                    </tr>  
                </thead>  
            <tbody>  
                { this.vaccinationRow() }   
            </tbody>  
        </table>  
    </div>
  )
}
}